<?php

namespace App\ValueObject;

class Calculation
{
    /**
     * @var float
     */
    private $interestRate;

    /**
     * @var float
     */
    private $annualPercentageRate;

    public function getInterestRate()
    {
        return $this->interestRate;
    }

    public function setInterestRate($interestRate)
    {
        $this->interestRate = $interestRate;

        return $this;
    }

    public function getAnnualPercentageRate()
    {
        return $this->annualPercentageRate;
    }

    public function setAnnualPercentageRate($annualPercentageRate)
    {
        $this->annualPercentageRate = $annualPercentageRate;

        return $this;
    }
}