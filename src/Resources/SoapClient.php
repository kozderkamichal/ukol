<?php

namespace App\Resources;

use App\DTO\LoanRequest;
use App\Helper\Curl;
use App\ValueObject\Calculation;

class SoapClient implements ResourceClientInterface
{
    /**
     * @var string
     */
    private $name = "SOAP";

    /**
     * @var Curl
     */
    private $curl;

    private $urlForHash = "http://www.toppojisteni.net/zadani/rest/rc.php";

    private $wsdlUrl = "http://www.toppojisteni.net/zadani/soap/server.php?wsdl";

    private $url = "http://www.toppojisteni.net/zadani/soap/server.php";

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }

    public function calculate(LoanRequest $loanRequest): Calculation
    {
        $hash = $this->curl->get($this->urlForHash, null, $this->createHashRequest($loanRequest));

        $soapClient = new \SoapClient($this->wsdlUrl, $this->getSoapOptions());

        if (!$hash->hash) {
            throw new \Exception("Hash neexistuje");
        }

        $result = $soapClient->Calc($this->createCalculationRequest($loanRequest, $hash->hash));


        if (property_exists($result, "result") && $result->error) {
            throw new \Exception($result->error->string);
        }

        $calculation = new Calculation();

        if (!property_exists($result, "interest_rate") || !property_exists($result, "rpsn")) {
            throw new \Exception("Chyba ve výpočtu");
        }

        $calculation->setInterestRate($result->interest_rate)
            ->setAnnualPercentageRate($result->rpsn);

        return $calculation;
    }

    private function createCalculationRequest(LoanRequest $loanRequest, string $hash): \stdClass
    {
        return (object)[
            "clientScoringHash" => $hash,
            "amount" => (int)$loanRequest->getLoanAmount(),
            "house_value" => (int)$loanRequest->getPropertyPrice(),
            "repayment_time" => (int)$loanRequest->getRepaymentPeriod(),
            "fixation" => $loanRequest->getFixationTime()
        ];
    }

    private function getSoapOptions(): array
    {
        return [
            "location" => $this->url
        ];
    }

    private function createHashRequest(LoanRequest $loanRequest): array
    {
        return [
            "rc" => $loanRequest->getPersonalIdentificationNumber()
        ];
    }

    public function getResourceName(): string
    {
        return $this->name;
    }
}