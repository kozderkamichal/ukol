<?php

namespace App\Resources;

use App\DTO\LoanRequest;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpKernel\KernelInterface;
use App\ValueObject\Calculation;

class XlsxClient implements ResourceClientInterface
{

    private $name = "XLSX";

    private $url = "http://www.toppojisteni.net/zadani/sazby.xlsx";

    private $cacheDirectory;

    public function __construct(KernelInterface $kernel)
    {
        $this->cacheDirectory = $kernel->getProjectDir() . "/var";
    }

    public function calculate(LoanRequest $loanRequest): Calculation
    {
        $this->downloadFile();

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($this->getFilePath());
        $sheet = $spreadsheet->getSheet(0);
        $tableFirstRow = $this->getTableFirstRowTableByLoanRequest($sheet, $loanRequest);
        $fixationRow = $this->getFixationRowByLoanRequest($sheet, $loanRequest, $tableFirstRow);
        $repaymentPeriodColumn = $this->getRepaymentPeriodColumnByLoanRequest($sheet, $loanRequest, $tableFirstRow);

        $calculation = new Calculation();
        $calculation->setInterestRate($sheet->getCellByColumnAndRow($repaymentPeriodColumn, $fixationRow)->getValue())
            ->setAnnualPercentageRate($sheet->getCellByColumnAndRow($repaymentPeriodColumn + 1, $fixationRow)->getValue());

        return $calculation;
    }

    private function getRepaymentPeriodColumnByLoanRequest(Worksheet $sheet, LoanRequest $loanRequest, int $tableFirstRow)
    {
        $highestColumn = $sheet->getHighestColumn($tableFirstRow);
        $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);

        for ($column = 2; $column <= $highestColumnIndex; $column++) {
            $repaymentPeriod = $sheet->getCellByColumnAndRow($column, $tableFirstRow)->getValue();

            if ($repaymentPeriod >= $loanRequest->getRepaymentPeriod()) {
                return $column;
            }
        }

        throw new \Exception("Půjčka neexistuje");
    }

    private function getFixationRowByLoanRequest(Worksheet $sheet, LoanRequest $loanRequest, int $tableFirstRow)
    {
        $highestRow = $sheet->getHighestRow();

        for ($row = $tableFirstRow; $row <= $highestRow; $row++) {
            $fixationTime = $sheet->getCellByColumnAndRow(1, $row)->getValue();

            if (!$fixationTime) {
                break;
            }

            if ($fixationTime == $loanRequest->getFixationTime()) {
                return $row;
            }
        }

        throw new \Exception("Půjčka neexistuje");
    }

    private function getTableFirstRowTableByLoanRequest(Worksheet $sheet, LoanRequest $loanRequest): int
    {
        $highestRow = $sheet->getHighestRow();
        $isPreviousCellEmpty = true;

        for ($row = 1; $row <= $highestRow; $row++) {
            $loanAmount = $sheet->getCellByColumnAndRow(1, $row)->getValue();

            if ($isPreviousCellEmpty) {
                if ($loanAmount >= $loanRequest->getLoanAmount()) {
                    return $row;
                }
            }

            $isPreviousCellEmpty = false;

            if (!$loanAmount) {
                $isPreviousCellEmpty = true;
            }
        }

        throw new \Exception("Půjčka neexistuje");
    }

    private function downloadFile(): void
    {
        file_put_contents($this->getFilePath(), file_get_contents($this->url));
    }

    private function getFilePath(): string
    {
        return $this->cacheDirectory . "/data.xlsx";
    }

    public function getResourceName(): string
    {
        return $this->name;
    }
}