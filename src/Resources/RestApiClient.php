<?php

namespace App\Resources;

use App\DTO\LoanRequest;
use App\Helper\Curl;
use App\ValueObject\Calculation;

class RestApiClient implements ResourceClientInterface
{
    /**
     * @var string
     */
    private $name = "Rest API";

    /**
     * @var string
     */
    private $url = "http://www.toppojisteni.net/zadani/rest/institution.php";

    /**
     * @var Curl
     */
    private $curl;

    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }

    public function calculate(LoanRequest $loanRequest): Calculation
    {
        $items = $this->curl->get($this->url, $this->createRestRequest($loanRequest));

        if (property_exists($items, "stat")) {
            throw new \Exception($items->stat);
        }

        if (property_exists($items, "res") && is_array($items->res)) {
            foreach ($items->res as $item) {
                if ($item->fix == $loanRequest->getFixationTime()) {
                    $calculation = new Calculation();

                    $calculation->setAnnualPercentageRate($item->rpsn)
                        ->setInterestRate($item->intRate);

                    return $calculation;
                }
            }
        }

        throw new \Exception("Žádný výsledek");
    }

    private function createRestRequest(LoanRequest $loanRequest): string
    {
        return json_encode((object)[
            "Amount" => $loanRequest->getLoanAmount(),
            "RepTime" => $loanRequest->getRepaymentPeriod()
        ]);
    }

    public function getResourceName(): string
    {
        return $this->name;
    }
}