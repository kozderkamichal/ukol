<?php

namespace App\Resources;

use App\DTO\LoanRequest;
use App\ValueObject\Calculation;

interface ResourceClientInterface
{
    public function calculate(LoanRequest $loanRequest): Calculation;

    public function getResourceName(): string;
}