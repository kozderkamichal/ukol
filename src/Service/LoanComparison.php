<?php

namespace App\Service;


use App\DTO\ComparisonResult;
use App\DTO\LoanRequest;
use App\Resources\ResourceClientInterface;
use App\Resources\RestApiClient;
use App\Resources\SoapClient;
use App\Resources\XlsxClient;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class LoanComparison
{
    /**
     * @var ResourceClientInterface[]
     */
    private $clients = [];

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface
     */
    private $flashBag;

    public function __construct(
        RestApiClient $restApiClient,
        SoapClient $soapClient,
        XlsxClient $xlsxClient,
        SessionInterface $session
    ) {
        $this->clients[] = $restApiClient;
        $this->clients[] = $soapClient;
        $this->clients[] = $xlsxClient;

        $this->flashBag = $session->getFlashBag();
    }

    public function compare(LoanRequest $loanRequest): array
    {
        $results = [];

        foreach ($this->clients as $client) {
            try {
                $comparisonResult = new ComparisonResult();

                $comparisonResult->setCalculation($client->calculate($loanRequest))
                    ->setResource($client->getResourceName());

                $results[] = $comparisonResult;
            } catch (\Exception $e) {
                $this->flashBag->add("error", $e->getMessage());
            }
        }

        return $results;
    }
}