<?php

namespace App\Controller;

use App\Form\LoanRequest;
use App\Service\LoanComparison;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LoanController extends AbstractController
{
    private $loanComparisonService;

    public function __construct(LoanComparison $loanComparisonService)
    {
        $this->loanComparisonService = $loanComparisonService;
    }

    /**
     * @Route("/", name="loan")
     */
    public function index(Request $request)
    {
        $loanRequest = new \App\DTO\LoanRequest();

        $form = $this->createForm(
            LoanRequest::class,
            $loanRequest,
            [
                "method" => "POST",
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $request->getSession()->set("loanRequest", $loanRequest);

            return $this->redirectToRoute("compare");
        }

        return $this->render("loan/index.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/porovnani-pujcek", name="compare")
     */
    public function compare(Request $request)
    {
        $session = $request->getSession();

        $loanRequest = $session->get("loanRequest");

        if (!$loanRequest) {
            return $this->redirectToRoute("loan");
        }

        $session->remove("loanRequest");

        $results = $this->loanComparisonService->compare($loanRequest);

        return $this->render("loan/compare.html.twig", [
            "items" => $results,
        ]);
    }
}
