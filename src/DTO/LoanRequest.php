<?php

namespace App\DTO;

class LoanRequest
{

    private $propertyPrice;
    private $loanAmount;
    private $repaymentPeriod;
    private $fixationTime;
    private $personalIdentificationNumber;

    /**
     * @return mixed
     */
    public function getPropertyPrice()
    {
        return $this->propertyPrice;
    }

    /**
     * @param mixed $propertyPrice
     */
    public function setPropertyPrice($propertyPrice): void
    {
        $this->propertyPrice = $propertyPrice;
    }

    /**
     * @return mixed
     */
    public function getLoanAmount()
    {
        return $this->loanAmount;
    }

    /**
     * @param mixed $loanAmount
     */
    public function setLoanAmount($loanAmount): void
    {
        $this->loanAmount = $loanAmount;
    }

    /**
     * @return mixed
     */
    public function getRepaymentPeriod()
    {
        return $this->repaymentPeriod;
    }

    /**
     * @param mixed $repaymentPeriod
     */
    public function setRepaymentPeriod($repaymentPeriod): void
    {
        $this->repaymentPeriod = $repaymentPeriod;
    }

    /**
     * @return mixed
     */
    public function getFixationTime()
    {
        return $this->fixationTime;
    }

    /**
     * @param mixed $fixationTime
     */
    public function setFixationTime($fixationTime): void
    {
        $this->fixationTime = $fixationTime;
    }

    /**
     * @return mixed
     */
    public function getPersonalIdentificationNumber()
    {
        return $this->personalIdentificationNumber;
    }

    /**
     * @param mixed $personalIdentificationNumber
     */
    public function setPersonalIdentificationNumber($personalIdentificationNumber): void
    {
        $this->personalIdentificationNumber = $personalIdentificationNumber;
    }
}