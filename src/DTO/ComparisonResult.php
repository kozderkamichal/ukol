<?php

namespace App\DTO;

use App\ValueObject\Calculation;

class ComparisonResult
{
    /**
     * @var Calculation
     */
    private $calculation;

    /**
     * @var string
     */
    private $resource;

    public function getCalculation(): Calculation
    {
        return $this->calculation;
    }

    public function setCalculation(Calculation $calculation): ComparisonResult
    {
        $this->calculation = $calculation;

        return $this;
    }

    public function getResource(): string
    {
        return $this->resource;
    }

    public function setResource(string $resource): ComparisonResult
    {
        $this->resource = $resource;

        return $this;
    }


}