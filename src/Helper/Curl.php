<?php

namespace App\Helper;

class Curl
{
    public function get(string $url, string $body = null, array $params = []): \stdClass
    {
        $ch = curl_init();

        if ($params) {
            $url .= "?" . http_build_query($params);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($body) {
            curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
        }

        $response = curl_exec($ch);

        curl_close($ch);

        $decodedResponse = json_decode($response);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \Exception("Chybná odpověď ze serveru");
        }

        return $decodedResponse;
    }
}