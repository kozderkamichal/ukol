<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class LoanRequest extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $repaymentPeriodRange = range(1, 30, 1);
        $fixationTimePeriod = [1, 3, 5, 10];

        $builder->add(
                "propertyPrice",
                MoneyType::class,
                [
                    "required" => true,
                    "currency" => "CZK",
                    "label" => "Kupní cena nemovitosti (do 10 000 000 Kč)",
                    "constraints" => [
                        new LessThanOrEqual([
                            "value" => 10000000,
                        ]),
                        new GreaterThan([
                            "value" => 0
                        ])
                    ]
                ])
            ->add(
                "loanAmount",
                MoneyType::class,
                [
                    "required" => true,
                    "currency" => "CZK",
                    "label" => "Kolik si chci pujčit (do 10 000 000 Kč)",
                    "constraints" => [
                        new LessThanOrEqual([
                            "value" => 10000000,
                        ]),
                        new GreaterThan([
                            "value" => 0
                        ])
                    ]
                ])
            ->add(
                "repaymentPeriod",
                ChoiceType::class,
                [
                    "label" => "Doba splácení",
                    "choices" => array_combine($repaymentPeriodRange, $repaymentPeriodRange),
                    "required" => true,
                ])
            ->add(
                "fixationTime",
                ChoiceType::class,
                [
                    "label" => "Doba fixace",
                    "choices" => array_combine($fixationTimePeriod, $fixationTimePeriod),
                    "required" => true,
                ])
            ->add(
                "personalIdentificationNumber",
                TextType::class,
                [
                    "label" => "Rodné číslo (bez lomítka \"/\")",
                    "required" => true,
                ])
            ->add(
                "compare",
                SubmitType::class,
                [
                    "label" => "Porovnej"
                ]
            );
    }

}