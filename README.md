# Úkol
Úkol je napsaný v symfony, abych si ho rovnou i vyzkoušel. Neřešil jsem překlady, proto jsou některé řetězce v češtině a validační hlášky u formuláře v angličtině.

Vím, že je v tom programu nutné mít daleko více validací, třeba na správný formát rodného čísla, kontrola doby fixa, doby splácení, atd... tak jsem jich udělal jen pár. Pokud budete chtít dodělám i další validace.
